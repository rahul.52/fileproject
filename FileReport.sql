USE [FileReport]
GO
/****** Object:  Table [dbo].[FileData]    Script Date: 18/04/19 4:27:00 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[FileData](
	[SN] [nchar](10) NULL,
	[PhotoPath] [nvarchar](max) NULL,
	[PhotoName] [nvarchar](max) NULL,
	[CertificatePath] [nvarchar](max) NULL,
	[CertificateName] [nvarchar](max) NULL,
	[ShowPhotoPath] [nvarchar](max) NULL,
	[ShowCertificatePath] [nvarchar](max) NULL
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
