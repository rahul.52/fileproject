﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FileProject.Models;
using Dapper;
using System.Data;
using System.Data.SqlClient;
using System.Configuration;
using System.IO;
using System.Net;

namespace FileProject.Controllers
{
    public class SaveFileController : Controller
    {
        // GET: SaveFile
        public ActionResult Index()
        {
            return View();
        }


        public ActionResult Files()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Files(FileData data, string Save)
        {

            //Checking Database for MAX SN using dapper.
            int tempSN;
            try
            {
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    tempSN = db.Query<FileData>(@"select Top 1 SN from FileData Order By SN Desc;").SingleOrDefault().SN;
                }
            }
            //if no data TempSN = 1

            catch (Exception)
            {
                tempSN = 0;
            }

            //creating Random number
            //Random rnd = new Random();
            //int rand = rnd.Next(1, 5);


            int SN = tempSN + 1;
            string SNstring = Convert.ToString(SN);

            //path is defined in web.config     <add key="UserImagePath" value="D:\FileProject\FileProject\StorageFile\" />
            string UploadPath = ConfigurationManager.AppSettings["FilePath"].ToString();
            string ShowPath = ConfigurationManager.AppSettings["ShowPath"].ToString();

            string PhotoFileName = "";
            string CertificateFileName = "";

            string PhotoExtension = "";
            string CertificateExtension = "";

            if (data.PhotoFile != null)
            {
                PhotoExtension = Path.GetExtension(data.PhotoFile.FileName);
                PhotoFileName = SNstring + "-" + "PhotoFile" + PhotoExtension;
                data.PhotoPath = UploadPath + PhotoFileName;
                data.ShowPhotoPath = ShowPath + PhotoFileName;

            }
            if (data.CertificateFile != null)
            {
                CertificateExtension = Path.GetExtension(data.CertificateFile.FileName);
                CertificateFileName = SNstring + "-" + "CertificateFile" + CertificateExtension;
                data.CertificatePath = UploadPath + CertificateFileName;
                data.ShowCertificatePath = ShowPath + CertificateFileName;

            }

            string photoPath = data.PhotoPath;
            string certificatePath = data.CertificatePath;
            string showPhotoPath = data.ShowPhotoPath;
            string showCertificatePath = data.ShowCertificatePath;

            if (Save != null)
            {
                //storing location of data in Database.



                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {

                    db.Query<FileData>(@"INSERT INTO FileData (SN, PhotoPath,PhotoName, CertificatePath, CertificateName, ShowPhotoPath,ShowCertificatePath) VALUES (@SN, @photoPath, @PhotoFileName, @certificatePath,@CertificateFileName,@showPhotoPath,@showCertificatePath)", new { SN, photoPath, PhotoFileName, certificatePath , CertificateFileName, showPhotoPath, showCertificatePath });

                }

                //Storing data in folder.

                if (data.PhotoFile != null)
                {
                    data.PhotoFile.SaveAs(data.PhotoPath);
                }
                if (data.CertificateFile != null)
                {
                    data.CertificateFile.SaveAs(data.CertificatePath);
                }

                return View("Success");
            }
            return View();
        }


        public ActionResult Detail(string Btn, int? SN, int? stat)
        {
            try
            {
                int tsn;
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    tsn = db.Query<FileData>(@"Select * from FileData where SN=@SN", new { SN }).SingleOrDefault().SN;
                }
            }
            catch (Exception)
            {
                stat = 1;
            }
            if (Btn != null)
            {
                if(stat ==1)
                {
                    FileData fileData = new FileData();
                    fileData.Error = "Enter Correct SN.";
                    return View(fileData);
                }
                return RedirectToAction("Show", new { SN });
            }
            
            return View();
        }
        public ActionResult Show(int? SN, string saveBtn , FileData data , string cnBtn)
        {
            FileData fileData = new FileData();
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                fileData = db.Query<FileData>(@"Select * From FileData where SN = @SN", new { SN }).SingleOrDefault();
            }

            if(saveBtn != null)
            {
                string UploadPath = ConfigurationManager.AppSettings["FilePath"].ToString();
                string ShowPath = ConfigurationManager.AppSettings["ShowPath"].ToString();

                string SNstring = Convert.ToString(SN);

                if (data.PhotoFile != null)
                {
                    var path = fileData.PhotoPath;
                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                        using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                        {
                            db.Query<FileData>(@"Update FileData Set PhotoPath = NULL, ShowPhotoPath = NULL, PhotoName = NULL where SN=@SN", new { SN });
                        }
                    }

                    string PhotoFileName = "";
                    string PhotoExtension = "";
                    PhotoExtension = Path.GetExtension(data.PhotoFile.FileName);
                    PhotoFileName = SNstring + "-" + "PhotoFile" + PhotoExtension;
                    data.PhotoPath = UploadPath + PhotoFileName;
                    data.ShowPhotoPath = ShowPath + PhotoFileName;

                    string photoPath = data.PhotoPath;
                    string showPhotoPath = data.ShowPhotoPath;

                    using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                    {
                        db.Query<FileData>(@"Update FileData Set PhotoPath = @photoPath, PhotoName = @PhotoFileName, ShowPhotoPath = @showPhotoPath where SN = @SN", new { photoPath, PhotoFileName, showPhotoPath, SN });
                    }
                    data.PhotoFile.SaveAs(data.PhotoPath);
                }
                if (data.CertificateFile != null)
                {
                    var path = fileData.CertificatePath;
                    if (System.IO.File.Exists(path))
                    {
                        System.IO.File.Delete(path);
                        using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                        {
                            db.Query<FileData>(@"Update FileData Set CertificatePath = NULL, ShowCertificatePath = NULL, CertificateName = NULL where SN=@SN", new { SN });
                        }
                    }
                    string CertificateFileName = "";
                    string CertificateExtension = "";
                    CertificateExtension = Path.GetExtension(data.CertificateFile.FileName);
                    CertificateFileName = SNstring + "-" + "CertificateFile" + CertificateExtension;
                    data.CertificatePath = UploadPath + CertificateFileName;
                    data.ShowCertificatePath = ShowPath + CertificateFileName;

                    string showCertificatePath = data.ShowCertificatePath;
                    string certificatePath = data.CertificatePath;
                    using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                    {
                        db.Query<FileData>(@"Update FileData Set CertificatePath = @certificatePath, CertificateName = @CertificateFileName, ShowCertificatePath = @showCertificatePath where SN = @SN", new { certificatePath, CertificateFileName, showCertificatePath, SN });
                    }
                    data.CertificateFile.SaveAs(data.CertificatePath);
                }
                return View("Detail");


            }
            if(cnBtn!= null)
            {
                return RedirectToAction("Detail");
            }
            return View(fileData);
        }

        [HttpPost]
        public JsonResult DeleteFile(string SN)
        {
            FileData fileData = new FileData();
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                fileData = db.Query<FileData>(@"Select * From FileData where SN = @SN", new {SN }).SingleOrDefault();
            }
            var path = fileData.PhotoPath;
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                { 
                    db.Query<FileData>(@"Update FileData Set PhotoPath = NULL, ShowPhotoPath = NULL, PhotoName = NULL where SN=@SN", new { SN});
                }
            }
            return Json(new { Result = "OK" });
        }

        
        [HttpPost]
        public JsonResult DeleteCertificate(string SN)
        {
            FileData fileData = new FileData();
            using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
            {
                fileData = db.Query<FileData>(@"Select * From FileData where SN = @SN", new { SN }).SingleOrDefault();
            }
            var path = fileData.CertificatePath;
            if (System.IO.File.Exists(path))
            {
                System.IO.File.Delete(path);
                using (IDbConnection db = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString))
                {
                    db.Query<FileData>(@"Update FileData Set CertificatePath = NULL, ShowCertificatePath = NULL, CertificateName = NULL where SN=@SN", new { SN });
                }
            }
            return Json(new { Result = "OK" });
        }
    }
}