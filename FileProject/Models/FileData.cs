﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FileProject.Models
{
    public class FileData
    {
        public int SN { get; set; }
        [Display(Name ="Photo")]
        public string PhotoPath { get; set; } = null;
        [Display(Name = "Certificate")]
        public string CertificatePath { get; set; } = null;

        public HttpPostedFileBase PhotoFile { get; set; }

        public HttpPostedFileBase CertificateFile { get; set; }

        public string PhotoName { get; set; } = null;
        public string CertificateName { get; set; } = null;

        public string ShowPhotoPath { get; set; } = null;
        public string ShowCertificatePath { get; set; } = null;

        public string Error{ get; set; }
    }
}